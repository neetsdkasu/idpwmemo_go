module bitbucket.org/neetsdkasu/idpwmemo_go

go 1.15

require (
	bitbucket.org/neetsdkasu/java_data_io_go v1.1.2
	bitbucket.org/neetsdkasu/mersenne_twister_go v1.2.0
	bitbucket.org/neetsdkasu/unkocrypto_go/v2 v2.0.0
)
