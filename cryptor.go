package idpwmemo_go

import (
	mt "bitbucket.org/neetsdkasu/mersenne_twister_go"
	unko "bitbucket.org/neetsdkasu/unkocrypto_go/v2"
	"bytes"
	"errors"
	"hash"
	"hash/crc32"
	"io"
)

const cryptorVersion = 2

const maxBlockSize = 1024 // 1024 < unko.MaxBlockSize

var FailDecrypt = errors.New("failed decrypt")

type cryptor struct {
	rng *mt.MersenneTwister
	cs  hash.Hash
}

func newCryptor() *cryptor {
	return &cryptor{
		mt.NewMersenneTwister(),
		crc32.NewIEEE(),
	}
}

func genInitialSeed(size int) []uint32 {
	seed := make([]uint32, size)
	seed[0] = 0x98765432
	seed[1] = 0xF1E2D3C4
	for i := 2; i < len(seed); i++ {
		seed[i] = seed[i-2] ^ (seed[i-1] >> ((i - 1) & 0xF)) ^ (seed[i-1] << ((i + i + 1) & 0xF))
	}
	return seed
}

func genSeedV1(password []byte) []uint32 {
	if len(password) == 0 {
		return genInitialSeed(23)
	}
	seed := genInitialSeed(len(password) + 13)
	var p = 0
	for i := range seed {
		for j := 0; j < 4; j++ {
			seed[i] |= uint32(int32(int8(password[p])) << (((j + i*i) & 3) << 3))
			p++
			if p >= len(password) {
				p = 0
			}
		}
	}
	return seed
}

func encryptoBlockSize(srcLen int) int {
	var size = srcLen + unko.MetaSize
	if size <= unko.MinBlockSize {
		return unko.MinBlockSize
	}
	if size <= maxBlockSize {
		return size
	}
	size = unko.MinBlockSize
	var blockCount = (srcLen + (size - unko.MetaSize) - 1) / (size - unko.MetaSize)
	var totalSize = size * blockCount
	for sz := unko.MinBlockSize + 1; sz <= maxBlockSize; sz++ {
		blockCount = (srcLen + (sz - unko.MetaSize) - 1) / (sz - unko.MetaSize)
		if sz*blockCount < totalSize {
			size = sz
			totalSize = sz * blockCount
		}
	}
	return size
}

func (self *cryptor) decryptV1(password, src []byte) ([]byte, error) {
	var seed = genSeedV1(password)
	var size = len(src)
	if size > maxBlockSize {
		size = maxBlockSize
	}
	var reader = bytes.NewReader(src)
	var dst bytes.Buffer
	for ; size >= unko.MinBlockSize; size-- {
		if len(src)%size != 0 {
			continue
		}
		_, err := reader.Seek(0, io.SeekStart)
		if err != nil {
			return nil, err
		}
		dst.Reset()
		self.rng.InitByArray(seed)
		_, err = unko.Decrypt(size, self.cs, self, reader, &dst)
		if err == nil {
			return dst.Bytes(), nil
		}
		if _, ok := err.(unko.UnkocryptoError); !ok {
			return nil, err
		}
	}
	return nil, FailDecrypt
}

func (self *cryptor) encryptV1(password, src []byte) ([]byte, error) {
	var seed = genSeedV1(password)
	var size = encryptoBlockSize(len(src))
	var reader = bytes.NewReader(src)
	var dst bytes.Buffer
	self.rng.InitByArray(seed)
	_, err := unko.Encrypt(size, self.cs, self, reader, &dst)
	if err != nil {
		return nil, err
	}
	return dst.Bytes(), nil
}

func (self *cryptor) decryptRepeatV1(n int, password, src []byte) (ret []byte, err error) {
	for i := 0; i < n; i++ {
		ret, err = self.decryptV1(password, src)
		if err != nil {
			return nil, err
		}
		src = ret
	}
	return
}

func (self *cryptor) encryptRepeatV1(n int, password, src []byte) (ret []byte, err error) {
	for i := 0; i < n; i++ {
		ret, err = self.encryptV1(password, src)
		if err != nil {
			return nil, err
		}
		src = ret
	}
	return
}

func checkSrcType(src []byte) int {
	srcType := 0
	var version byte = 0
	for _, r := range src[:8] {
		version ^= r
	}
	if version == cryptorVersion {
		dataLen := len(src) - 1
		size2 := dataLen
		if size2 > maxBlockSize {
			size2 = maxBlockSize
		}
		for ; size2 >= unko.MinBlockSize; size2-- {
			if dataLen%size2 == 0 {
				srcType = 2
				break
			}
		}
	}
	size1 := len(src)
	if size1 > maxBlockSize {
		size1 = maxBlockSize
	}
	for ; size1 >= unko.MinBlockSize; size1-- {
		if len(src)%size1 == 0 {
			srcType |= 1
			break
		}
	}
	return srcType
}

func genSeedV2(password []byte) []uint32 {
	if len(password) == 0 {
		return genInitialSeed(37)
	}
	seed := genInitialSeed(len(password) + 29)
	var p = 0
	for i := range seed {
		for j := 0; j < 4; j++ {
			seed[i] ^= uint32(password[p]) << (((j + i*i) & 3) << 3)
			p++
			if p >= len(password) {
				p = 0
			}
		}
	}
	return seed
}

func (self *cryptor) decryptV2(password, src []byte) ([]byte, error) {
	var version byte = 0
	for _, r := range src[:8] {
		version ^= r
	}
	if version != cryptorVersion {
		return nil, FailDecrypt
	}
	var seed = genSeedV2(password)
	var dataLen = len(src) - 1
	var size1 = dataLen
	if size1 > maxBlockSize {
		size1 = maxBlockSize
	}
	var reader = bytes.NewReader(src[1:])
	var dst1 bytes.Buffer
	var dst2 bytes.Buffer
	for ; size1 >= unko.MinBlockSize; size1-- {
		if dataLen%size1 != 0 {
			continue
		}
		_, err := reader.Seek(0, io.SeekStart)
		if err != nil {
			return nil, err
		}
		dst1.Reset()
		self.rng.InitByArray(seed)
		_, err = unko.Decrypt(size1, self.cs, self, reader, &dst1)
		if err != nil {
			if _, ok := err.(unko.UnkocryptoError); !ok {
				return nil, err
			}
			continue
		}
		src2 := dst1.Bytes()
		var size2 = len(src2)
		if size2 > maxBlockSize {
			size2 = maxBlockSize
		}
		var reader2 = bytes.NewReader(src2)
		for ; size2 >= unko.MinBlockSize; size2-- {
			if len(src2)%size2 != 0 {
				continue
			}
			_, err = reader2.Seek(0, io.SeekStart)
			if err != nil {
				return nil, err
			}
			dst2.Reset()
			self.rng.InitByArray(seed)
			_, err = unko.Decrypt(size2, self.cs, self, reader2, &dst2)
			if err == nil {
				return dst2.Bytes(), nil
			}
			if _, ok := err.(unko.UnkocryptoError); !ok {
				return nil, err
			}
		}
	}
	return nil, FailDecrypt
}

func (self *cryptor) encryptV2(password, src []byte) ([]byte, error) {
	var seed = genSeedV2(password)
	var size = encryptoBlockSize(len(src))
	var reader = bytes.NewReader(src)
	var dst1 bytes.Buffer
	var dst2 bytes.Buffer
	self.rng.InitByArray(seed)
	_, err := unko.Encrypt(size, self.cs, self, reader, &dst1)
	if err != nil {
		return nil, err
	}
	src = dst1.Bytes()
	size = encryptoBlockSize(len(src))
	reader = bytes.NewReader(src)
	dst2.WriteByte(cryptorVersion)
	self.rng.InitByArray(seed)
	_, err = unko.Encrypt(size, self.cs, self, reader, &dst2)
	if err != nil {
		return nil, err
	}
	ret := dst2.Bytes()
	for _, r := range ret[1:8] {
		ret[0] ^= r
	}
	return ret, nil
}

func (self *cryptor) NextInt() int32 {
	return int32(self.rng.Uint32())
}
