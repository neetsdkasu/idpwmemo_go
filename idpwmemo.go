package idpwmemo_go

import (
	java "bitbucket.org/neetsdkasu/java_data_io_go"
	"bytes"
)

type Error struct {
	Cause string
}

func (self Error) Error() string {
	return "Error(" + self.Cause + ")"
}

type IDPWMemo struct {
	cryptor    *cryptor
	passwordV1 []byte
	passwordV2 []byte
	memo       *memo
	index      int
	secrets    []*Value
	version    int
}

type Values struct {
	values *[]*Value
}

func (self *Values) Len() int {
	return len(*self.values)
}

func (self *Values) Get(index int) *Value {
	return (*self.values)[index]
}

func (self *Values) Set(index int, newValue *Value) error {
	if index < 0 || index >= self.Len() {
		return Error{"index out of bound"}
	}
	(*self.values)[index] = newValue
	return nil
}

func (self *Values) Add(newValue *Value) {
	values := self.Slice()
	*self.values = append(values, newValue)
}

func (self *Values) Slice() []*Value {
	return *self.values
}

const notSelectIndex = -1

func NewIDPWMemo() *IDPWMemo {
	return &IDPWMemo{
		newCryptor(),
		nil,            // passwordV1
		nil,            // passwordV2
		nil,            // memo
		notSelectIndex, // index
		nil,            // secrets
		0,              // version
	}
}

func (self *IDPWMemo) Clear() {
	self.passwordV1 = nil
	self.passwordV2 = nil
	self.memo = nil
	self.index = notSelectIndex
	self.secrets = nil
	self.version = 0
}

func (self *IDPWMemo) SetPassword(password []byte) error {
	err := self.saveSecrets()
	if err != nil {
		return err
	}
	enc, err := self.cryptor.encryptV1([]byte{}, password)
	if err != nil {
		return err
	}
	key, err := self.cryptor.encryptV2(password, password)
	if err != nil {
		return err
	}
	self.passwordV1 = enc
	self.passwordV2 = key
	self.memo = nil
	self.index = notSelectIndex
	self.secrets = nil
	self.version = 0
	return nil
}

func (self *IDPWMemo) getPassword() ([]byte, error) {
	if self.passwordV1 == nil {
		return nil, Error{"no password"}
	}
	return self.cryptor.decryptV1([]byte{}, self.passwordV1)
}

func (self *IDPWMemo) NewMemo() error {
	if self.passwordV1 == nil {
		return Error{"no password"}
	}
	self.memo = newMemo()
	self.index = notSelectIndex
	self.secrets = nil
	self.version = 2
	return nil
}

func (self *IDPWMemo) LoadMemo(buf []byte) error {
	if self.passwordV1 == nil {
		return Error{"no password"}
	}
	err := self.saveSecrets()
	if err != nil {
		return err
	}
	srcType := checkSrcType(buf)
	if srcType == 0 {
		return Error{"unknown data"}
	}
	var buf2 []byte = nil
	if (srcType & 2) != 0 {
		buf2, err = self.cryptor.decryptV2(self.passwordV2, buf)
		if err == nil {
			self.version = 2
		} else if err != FailDecrypt || srcType == 2 {
			return err
		}
	}
	if buf2 == nil && (srcType&1) != 0 {
		dec, err := self.getPassword()
		if err != nil {
			return err
		}
		buf2, err = self.cryptor.decryptRepeatV1(2, dec, buf)
		if err != nil {
			return err
		}
		self.version = 1
	}
	reader := bytes.NewReader(buf2)
	memo, err := loadMemo(java.NewJavaDataInput(reader))
	if err != nil {
		return err
	}
	self.memo = memo
	self.index = notSelectIndex
	self.secrets = nil
	return nil
}

func (self *IDPWMemo) Services() ([]*Service, error) {
	if self.memo == nil {
		return nil, Error{"no memo"}
	} else {
		return self.memo.services, nil
	}
}

func (self *IDPWMemo) GetService(index int) (*Service, error) {
	if self.memo == nil {
		return nil, Error{"no memo"}
	} else if index < 0 || index >= len(self.memo.services) {
		return nil, Error{"wrong index"}

	} else {
		return self.memo.services[index], nil
	}
}

func (self *IDPWMemo) GetSelectedService() (*Service, error) {
	if self.index == notSelectIndex {
		return nil, Error{"not select service"}
	} else {
		return self.memo.services[self.index], nil
	}
}

func (self *IDPWMemo) ServiceNames() ([]string, error) {
	if self.memo == nil {
		return nil, Error{"no memo"}
	}
	names := make([]string, len(self.memo.services))
	for i, s := range self.memo.services {
		names[i] = s.GetServiceName()
	}
	return names, nil
}

func (self *IDPWMemo) AddNewService(serviceName string) error {
	err := self.saveSecrets()
	if err != nil {
		return err
	}
	if self.memo == nil {
		return Error{"no memo"}
	}
	service := NewService(serviceName)
	if service == nil {
		return Error{"serviceName is empty"}
	}
	services := self.memo.services
	self.index = len(services)
	self.memo.services = append(services, service)
	self.secrets = nil
	return nil
}

func (self *IDPWMemo) SelectService(index int) error {
	if self.memo == nil {
		return Error{"no memo"}
	}
	if index < 0 || index >= len(self.memo.services) {
		return Error{"wrong index"}
	}
	err := self.saveSecrets()
	if err != nil {
		return err
	}
	self.index = index
	self.secrets = nil
	return nil
}

func (self *IDPWMemo) Values() (*Values, error) {
	if self.index == notSelectIndex {
		return nil, Error{"not select service"}
	}
	return &Values{&self.memo.services[self.index].Values}, nil
}

func (self *IDPWMemo) Secrets() (*Values, error) {
	if self.secrets != nil {
		return &Values{&self.secrets}, nil
	}
	if self.index == notSelectIndex {
		return nil, Error{"not select service"}
	}
	var buf = self.memo.services[self.index].secrets[:]
	if len(buf) == 0 {
		self.secrets = []*Value{}
		return &Values{&self.secrets}, nil
	}
	var buf2 []byte = nil
	if self.version == 1 {
		dec, err := self.getPassword()
		if err != nil {
			return nil, err
		}
		buf2, err = self.cryptor.decryptRepeatV1(2, dec, buf)
		if err != nil {
			return nil, err
		}
	} else {
		var err error
		buf2, err = self.cryptor.decryptV2(self.passwordV2, buf)
		if err != nil {
			return nil, err
		}
	}
	var reader = bytes.NewReader(buf2)
	secrets, err := readSecrets(java.NewJavaDataInput(reader))
	if err != nil {
		return nil, err
	}
	self.secrets = secrets
	return &Values{&self.secrets}, nil
}

func (self *IDPWMemo) saveSecrets() error {
	if self.secrets == nil {
		return nil
	}
	var secrets = filterValues(self.secrets)
	if len(secrets) == 0 {
		self.memo.services[self.index].secrets = []byte{}
		return nil
	}
	var buf bytes.Buffer
	err := writeSecrets(java.NewJavaDataOutput(&buf), secrets)
	if err != nil {
		return err
	}
	var buf2 []byte = nil
	if self.version == 1 {
		dec, err := self.getPassword()
		if err != nil {
			return err
		}
		buf2, err = self.cryptor.encryptRepeatV1(2, dec, buf.Bytes())
		if err != nil {
			return err
		}
	} else {
		buf2, err = self.cryptor.encryptV2(self.passwordV2, buf.Bytes())
	}
	self.memo.services[self.index].secrets = buf2
	return nil
}

func (self *IDPWMemo) Save() ([]byte, error) {
	if self.memo == nil {
		return nil, Error{"no memo"}
	}
	err := self.saveSecrets()
	if err != nil {
		return nil, err
	}
	var buf bytes.Buffer
	err = self.memo.save(java.NewJavaDataOutput(&buf))
	if err != nil {
		return nil, err
	}
	if self.version == 1 {
		dec, err := self.getPassword()
		if err != nil {
			return nil, err
		}
		return self.cryptor.encryptRepeatV1(2, dec, buf.Bytes())
	} else {
		return self.cryptor.encryptV2(self.passwordV2, buf.Bytes())
	}
}

func (self *IDPWMemo) ChangePassword(newPassword []byte) error {
	if self.memo == nil {
		return Error{"no memo"}
	}
	self.saveSecrets()
	key, err := self.cryptor.encryptV2(newPassword, newPassword)
	if err != nil {
		return err
	}
	var dec []byte = nil
	if self.version == 1 {
		dec, err = self.getPassword()
		if err != nil {
			return err
		}
	}
	var tmp = make([][]byte, len(self.memo.services))
	for i, s := range self.memo.services {
		if len(s.secrets) == 0 {
			tmp[i] = []byte{}
			continue
		}
		if self.version == 1 {
			buf2, err := self.cryptor.decryptRepeatV1(2, dec, s.secrets)
			if err != nil {
				return err
			}
			tmp[i], err = self.cryptor.encryptRepeatV1(2, newPassword, buf2)
			if err != nil {
				return err
			}
		} else {
			buf2, err := self.cryptor.decryptV2(self.passwordV2, s.secrets)
			if err != nil {
				return err
			}
			tmp[i], err = self.cryptor.encryptV2(key, buf2)
			if err != nil {
				return err
			}
		}
	}
	enc, err := self.cryptor.encryptV1([]byte{}, newPassword)
	if err != nil {
		return err
	}
	self.passwordV1 = enc
	self.passwordV2 = key
	for i, s := range self.memo.services {
		s.secrets = tmp[i]
	}
	return nil
}
