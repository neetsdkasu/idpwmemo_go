package idpwmemo_go

import (
	java "bitbucket.org/neetsdkasu/java_data_io_go"
	"errors"
	"fmt"
	"log"
	"strings"
)

const memoFormatVersion = 2

var DataFormatError = errors.New("data format error")

type ValueType interface {
	fmt.Stringer
	AsInt() int
	valueType() valueType
}

type valueType int

func (self valueType) AsInt() int {
	return int(self)
}

func (self valueType) valueType() valueType {
	return self
}

func (self valueType) String() string {
	switch self {
	case vtServiceName:
		return "service name"
	case vtServiceUrl:
		return "service url"
	case vtId:
		return "id"
	case vtPassword:
		return "password"
	case vtEmail:
		return "email"
	case vtReminderQuestion:
		return "reminder question"
	case vtReminderAnswer:
		return "reminder answer"
	case vtDescription:
		return "description"
	default:
		log.Panicln("unknown valueType", int(self))
		return ""
	}
}

const (
	vtServiceName      valueType = iota // 0
	vtServiceUrl                        // 1
	vtId                                // 2
	vtPassword                          // 3
	vtEmail                             // 4
	vtReminderQuestion                  // 5
	vtReminderAnswer                    // 6
	vtDescription                       // 7
)

var (
	ServiceName      ValueType = vtServiceName
	ServiceUrl       ValueType = vtServiceUrl
	Id               ValueType = vtId
	Password         ValueType = vtPassword
	Email            ValueType = vtEmail
	ReminderQuestion ValueType = vtReminderQuestion
	ReminderAnswer   ValueType = vtReminderAnswer
	Description      ValueType = vtDescription
)

func ToValueType(v int) ValueType {
	if 0 <= v && v <= 7 {
		return valueType(v)
	} else {
		return nil
	}
}

type Value struct {
	ValueType
	Value string
}

type Service struct {
	Time    int64
	Values  []*Value
	secrets []byte
}

type memo struct {
	services []*Service
}

func NewValue(valueType ValueType, value string) *Value {
	if valueType == nil {
		return nil
	}
	return &Value{valueType, value}
}

func NewService(serviceName string) *Service {
	if len(strings.TrimSpace(serviceName)) == 0 {
		return nil
	}
	value := &Value{ServiceName, serviceName}
	return &Service{0, []*Value{value}, nil}
}

func newMemo() *memo {
	return &memo{nil}
}

func (self *Service) GetServiceName() string {
	for _, v := range self.Values {
		if v.ValueType == ServiceName {
			return v.Value
		}
	}
	return ""
}

func (self *Value) isEmpty() bool {
	return len(strings.TrimSpace(self.Value)) == 0
}

func filterValues(values []*Value) []*Value {
	var ret = make([]*Value, 0, len(values))
	for _, v := range values {
		if !v.isEmpty() {
			ret = append(ret, v)
		}
	}
	return ret
}

func readSecrets(src *java.JavaDataInput) ([]*Value, error) {
	vlen, err := src.ReadInt()
	if err != nil {
		return nil, err
	}
	if vlen < 0 {
		return nil, DataFormatError
	}
	ret := make([]*Value, vlen)
	for i := range ret {
		ret[i], err = loadValue(src)
		if err != nil {
			return nil, err
		}
	}
	return ret, nil
}

func writeSecrets(dst *java.JavaDataOutput, values []*Value) (err error) {
	err = dst.WriteInt(int32(len(values)))
	if err != nil {
		return
	}
	for _, v := range values {
		err = v.save(dst)
		if err != nil {
			return
		}
	}
	return
}

func loadValue(src *java.JavaDataInput) (*Value, error) {
	tmp, err := src.ReadByte()
	if err != nil {
		return nil, err
	}
	valueType := ToValueType(int(tmp))
	if valueType == nil {
		return nil, DataFormatError
	}
	value, err := src.ReadUTF()
	if err != nil {
		return nil, err
	}
	return &Value{
		valueType,
		java.FromJavaString(value),
	}, nil
}

func (self *Value) save(dst *java.JavaDataOutput) error {
	err := dst.WriteByte(int32(self.ValueType.AsInt()))
	if err != nil {
		return err
	}
	var s = java.ToJavaString(self.Value)
	return dst.WriteUTF(s)
}

func loadServiceV1(src *java.JavaDataInput) (*Service, error) {
	vlen, err := src.ReadInt()
	if err != nil {
		return nil, err
	}
	if vlen < 0 {
		return nil, DataFormatError
	}
	var values = make([]*Value, vlen)
	for i := range values {
		values[i], err = loadValue(src)
		if err != nil {
			return nil, err
		}
	}
	slen, err := src.ReadInt()
	if err != nil {
		return nil, err
	}
	if slen < 0 {
		return nil, DataFormatError
	}
	var secrets = make([]byte, slen)
	err = src.ReadFully(secrets)
	if err != nil {
		return nil, err
	}
	return &Service{0, values, secrets}, nil
}

func loadService(src *java.JavaDataInput) (*Service, error) {
	time, err := src.ReadLong()
	if err != nil {
		return nil, err
	}
	vlen, err := src.ReadInt()
	if err != nil {
		return nil, err
	}
	if vlen < 0 {
		return nil, DataFormatError
	}
	var values = make([]*Value, vlen)
	for i := range values {
		values[i], err = loadValue(src)
		if err != nil {
			return nil, err
		}
	}
	slen, err := src.ReadInt()
	if err != nil {
		return nil, err
	}
	if slen < 0 {
		return nil, DataFormatError
	}
	var secrets = make([]byte, slen)
	err = src.ReadFully(secrets)
	if err != nil {
		return nil, err
	}
	return &Service{time, values, secrets}, nil
}

func (self *Service) save(dst *java.JavaDataOutput) (err error) {
	err = dst.WriteLong(self.Time)
	if err != nil {
		return
	}
	var values = filterValues(self.Values)
	err = dst.WriteInt(int32(len(values)))
	if err != nil {
		return
	}
	for _, v := range values {
		err = v.save(dst)
		if err != nil {
			return
		}
	}
	err = dst.WriteInt(int32(len(self.secrets)))
	if err != nil {
		return
	}
	_, err = dst.Write(self.secrets)
	return
}

func loadMemo(src *java.JavaDataInput) (*memo, error) {
	version, err := src.ReadInt()
	if err != nil {
		return nil, err
	}
	if version < 0 || version > memoFormatVersion {
		return nil, DataFormatError
	}
	slen, err := src.ReadInt()
	if err != nil {
		return nil, err
	}
	if slen < 0 {
		return nil, DataFormatError
	}
	var services = make([]*Service, slen)
	for i := range services {
		if version == 1 {
			services[i], err = loadServiceV1(src)
		} else {
			services[i], err = loadService(src)
		}
		if err != nil {
			return nil, err
		}
	}
	return &memo{services}, nil
}

func (self *memo) save(dst *java.JavaDataOutput) (err error) {
	err = dst.WriteInt(memoFormatVersion)
	if err != nil {
		return
	}
	var services = make([]*Service, 0, len(self.services))
	for _, s := range self.services {
		if len(strings.TrimSpace(s.GetServiceName())) != 0 {
			services = append(services, s)
		}
	}
	err = dst.WriteInt(int32(len(services)))
	for _, s := range services {
		err = s.save(dst)
		if err != nil {
			return
		}
	}
	return
}
